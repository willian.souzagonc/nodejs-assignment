'use strict'

const dynamo = require('dynamodb')
const Joi = require('joi')
const config = require('../../configs').getProperties()

dynamo.AWS.config.update({
    region: config.dynamo.region,
    endpoint: config.dynamo.url,
    accessKeyId: config.aws.key_id,
    secretAccessKey: config.aws.secret_key,
})

const Loan = dynamo.define('Loan', {
    tableName: 'Loans',
    hashKey: 'id',
    timestamps: true,
    schema: {
        id: dynamo.types.uuid(),
        amount: Joi.number().required(),
        status: Joi.string(),
        company: Joi.object().required(),
    },
})

module.exports = {
    Loan,
}
