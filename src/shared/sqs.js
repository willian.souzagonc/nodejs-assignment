'use strict'

const AWS = require('aws-sdk')
const config = require('../../configs').getProperties()
AWS.config.setPromisesDependency(require('bluebird'))

const sqs = new AWS.SQS({
    apiVersion: '2012-11-05',
    sslEnabled: false,
    region: config.sqs.region,
    endpoint: config.sqs.url,
    accessKeyId: config.aws.key_id,
    secretAccessKey: config.aws.secret_key,
})

module.exports.sendToQueue = (message, queue) => {
    const params = {
        QueueUrl: `${config.sqs.url}/queue/${queue}`,
        MessageBody: JSON.stringify(message),
    }

    return sqs.sendMessage(params).promise()
}
