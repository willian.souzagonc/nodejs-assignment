module.exports.safelyJsonParse = data => {
    try {
        return JSON.parse(data)
    } catch (err) {
        return {}
    }
}

module.exports.wrapResponse = (statusCode, body) => {
    return {
        statusCode,
        body: JSON.stringify(typeof body === 'string' ? { message: body } : body),
    }
}
