const Joi = require('joi')

module.exports.validate = async (schema, data, options) => {
    return new Promise((resolve, reject) => {
        Joi.validate(data, schema, options, (err, value) => {
            if (err) {
                return reject(err)
            }

            return resolve(value)
        })
    })
}
