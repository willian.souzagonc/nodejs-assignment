const got = require('got')
const config = require('../../configs').getProperties()

const requestConfig = {
    json: true,
    headers: {
        'ovio-api-key': config.openkvk.key,
    },
}

module.exports.getCompanyById = async companyId => {
    try {
        const { body } = await got(`${config.openkvk.url}/openkvk/${companyId}`, requestConfig)

        return body
    } catch (err) {
        throw new Error('Error trying to get company by Id')
    }
}
