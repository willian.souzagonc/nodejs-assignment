const { wrapResponse } = require('../../shared/utils')

module.exports.handler = async event => {
    try {
        const { Loan } = require('../../shared/dynamo')
        const sqs = require('../../shared/sqs')

        const { id } = event.pathParameters

        let loan = await new Promise((resolve, reject) => {
            Loan.get(id, (err, loan) => {
                return err ? reject(err) : resolve(loan)
            })
        })

        if (!loan) {
            return wrapResponse(404, 'Loan not found')
        }

        loan = loan.toJSON()

        if (loan.status !== 'OFFERED') {
            return wrapResponse(
                422,
                `Not able to request disbursement. Reason: Status ${loan.status}`
            )
        }

        const [loanUpdated] = await Promise.all([
            Loan.update({ id: loan.id, status: 'WAITING_FOR_APPROVAL' }),
            sqs.sendToQueue(loan, 'DisbursementsRequested'),
        ])

        return wrapResponse(200, loanUpdated)
    } catch (e) {
        return wrapResponse(500, e.message)
    }
}
