const Joi = require('joi')
const { wrapResponse, safelyJsonParse } = require('../../shared/utils')
const validator = require('../../shared/validator')
const openKvk = require('../../shared/openkvk')

const loanSchema = Joi.object().keys({
    amount: Joi.number()
        .positive()
        .required(),
    company: Joi.object()
        .keys({
            id: Joi.string().required(),
        })
        .required(),
})

module.exports.handler = async event => {
    try {
        const { Loan } = require('../../shared/dynamo')

        let { amount, company } = await validator.validate(loanSchema, safelyJsonParse(event.body))

        company = await openKvk.getCompanyById(company.id)

        if (!company.actief) {
            return wrapResponse(422, 'Company must be active to register a loan')
        }

        let createdLoan = await Loan.create({ amount, status: 'OFFERED', company })

        return wrapResponse(201, createdLoan)
    } catch (e) {
        return wrapResponse(e.isJoi ? 400 : 500, e.message)
    }
}
