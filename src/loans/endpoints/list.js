const { wrapResponse } = require('../../shared/utils')

module.exports.handler = async () => {
    try {
        const { Loan } = require('../../shared/dynamo')

        let loans = await new Promise((resolve, reject) => {
            Loan.scan()
                .loadAll()
                .exec((err, loans) => {
                    return err ? reject(err) : resolve(loans.Items)
                })
        })

        return wrapResponse(200, loans)
    } catch (e) {
        return wrapResponse(500, e.message)
    }
}
