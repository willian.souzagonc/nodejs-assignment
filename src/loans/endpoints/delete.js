const { wrapResponse } = require('../../shared/utils')

module.exports.handler = async event => {
    try {
        const { Loan } = require('../../shared/dynamo')

        const { id } = event.pathParameters

        let deletedLoan = await Loan.destroy(id, { ReturnValues: 'ALL_OLD' })

        if (!deletedLoan) {
            return wrapResponse(404, 'Loan not found')
        }

        return wrapResponse(200, deletedLoan)
    } catch (e) {
        return wrapResponse(500, e.message)
    }
}
