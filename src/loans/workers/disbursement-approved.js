const { safelyJsonParse } = require('../../shared/utils')
const logger = require('../../shared/logger')

module.exports.handler = async event => {
    try {
        const { Loan } = require('../../shared/dynamo')

        await Promise.all(
            event.Records.map(record => {
                let { id } = safelyJsonParse(record.body)
                return Loan.update({ id, status: 'DISBURSED' })
            })
        )
    } catch (e) {
        //TODO: implement a queue for failed records
        logger.error('error processing approved disbursement', e)
    }
}
