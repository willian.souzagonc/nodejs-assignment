const { handler } = require('../../../loans/endpoints/list')

describe('Loan listing', () => {
    it('Listing all loans', async () => {
        const { statusCode, body } = await handler()

        expect(statusCode).toBe(200)
        expect(JSON.parse(body)).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    id: expect.any(String),
                    amount: expect.any(Number),
                    status: expect.any(String),
                    createdAt: expect.any(String),
                }),
            ])
        )
    })
})
