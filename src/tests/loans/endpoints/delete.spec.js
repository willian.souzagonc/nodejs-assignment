const { handler } = require('../../../loans/endpoints/delete')

describe('Loan deletion', () => {
    it('Deleting loan that does not exist, must return 404', async () => {
        const event = {
            pathParameters: { id: 'fake-id' },
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Deleting loan, must return 201 and deleted loan', async () => {
        const event = {
            pathParameters: { id: 'loan-to-be-deleted' },
        }

        const { statusCode, body } = await handler(event)

        expect(statusCode).toBe(200)
        expect(JSON.parse(body)).toEqual(
            expect.objectContaining({
                id: event.pathParameters.id,
                amount: expect.any(Number),
                status: expect.any(String),
                createdAt: expect.any(String),
            })
        )
    })
})
