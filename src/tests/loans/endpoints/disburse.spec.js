const { handler } = require('../../../loans/endpoints/disburse')

describe('Loan disbursement request', () => {
    it('Request disbursement for loan that does not exist, must return 404', async () => {
        const event = {
            pathParameters: { id: 'fake-id' },
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Request disbursement for loan already disbursed, must return 422 and current loan', async () => {
        const event = {
            pathParameters: { id: 'loan-disbursed' },
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Request disbursement for loan, must return 201 and loan updated', async () => {
        const event = {
            pathParameters: { id: 'loan-offered' },
        }

        const { statusCode, body } = await handler(event)

        expect(statusCode).toBe(200)
        expect(JSON.parse(body)).toEqual(
            expect.objectContaining({
                id: 'loan-offered',
                status: 'WAITING_FOR_APPROVAL',
            })
        )
    })
})
