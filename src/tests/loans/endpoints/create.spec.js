const { handler } = require('../../../loans/endpoints/create')
const config = require('../../../../configs').getProperties()
const nock = require('nock')

beforeAll(() => {
    nock(config.openkvk.url)
        .persist()
        .get(`/openkvk/active-company`)
        .reply(200, {
            slug: 'active-company',
            actief: true,
        })
        .persist()
        .get(`/openkvk/inactive-company`)
        .reply(200, {
            slug: 'inactive-company',
            actief: false,
        })
        .persist()
        .get(`/openkvk/fake-company`)
        .reply(500)
})

afterAll(() => {
    nock.cleanAll()
})

describe('Loan creation', () => {
    it('Creating loan with wrong amount, must return 400', async () => {
        const event = {
            body: JSON.stringify({ amount: 'string' }),
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Creating loan with company that does not exist, must return 500', async () => {
        const event = {
            body: JSON.stringify({
                amount: 100,
                company: { id: 'fake-company' },
            }),
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Creating loan with inactive company, must return 422', async () => {
        const event = {
            body: JSON.stringify({
                amount: 100,
                company: { id: 'inactive-company' },
            }),
        }

        expect(await handler(event)).toMatchSnapshot()
    })

    it('Creating loan with correct data, must return 201 and created loan', async () => {
        const amount = 100

        const event = {
            body: JSON.stringify({
                amount,
                company: { id: 'active-company' },
            }),
        }

        const { statusCode, body } = await handler(event)

        expect(statusCode).toBe(201)

        expect(JSON.parse(body)).toEqual(
            expect.objectContaining({
                id: expect.any(String),
                amount,
                status: 'OFFERED',
                company: expect.objectContaining({
                    slug: 'active-company',
                }),
                createdAt: expect.any(String),
            })
        )
    })
})
