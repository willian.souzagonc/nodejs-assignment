const { handler } = require('../../../loans/workers/disbursement-approved')
const { Loan } = require('../../../shared/dynamo')

describe('Process approved disbursements', () => {
    it('receive message with approved loan disbursement', async () => {
        const event = {
            Records: [
                {
                    body: '{"id":"loan-to-be-disbursed"}',
                },
            ],
        }

        await handler(event)

        let loan = await new Promise((resolve, reject) => {
            Loan.get('loan-to-be-disbursed', (err, loan) => {
                return err ? reject(err) : resolve(loan)
            })
        })

        expect(loan.toJSON()).toEqual(
            expect.objectContaining({
                status: 'DISBURSED',
            })
        )
    })
})
