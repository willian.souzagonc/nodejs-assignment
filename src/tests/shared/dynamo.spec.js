const { Loan } = require('../../shared/dynamo')

describe('Dynamo loan model', () => {
    it('loan model must be a object', () => {
        expect(Loan).toBeInstanceOf(Function)
    })

    it('loan model must have create function', async () => {
        expect(Loan).toHaveProperty('create')
        expect(Loan.create).toBeInstanceOf(Function)
    })

    it('loan model must have destroy function', async () => {
        expect(Loan).toHaveProperty('destroy')
        expect(Loan.destroy).toBeInstanceOf(Function)
    })

    it('loan model must have destroy function', async () => {
        expect(Loan).toHaveProperty('scan')
        expect(Loan.scan).toBeInstanceOf(Function)
    })
})
