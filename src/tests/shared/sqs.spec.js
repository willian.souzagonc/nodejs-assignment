const sqs = require('../../shared/sqs')

describe('SQS', () => {
    it('sqs must have sendToQueue method', () => {
        expect(sqs.sendToQueue).toBeInstanceOf(Function)
    })
})
