const { safelyJsonParse } = require('../../shared/utils')
const logger = require('../../shared/logger')

module.exports.handler = async event => {
    try {
        logger.info('Starting processing of disbursement requests')
        const sqs = require('../../shared/sqs')

        await Promise.all(
            event.Records.map(record =>
                sqs.sendToQueue(safelyJsonParse(record.body), 'DisbursementsApproved')
            )
        )
    } catch (err) {
        //TODO: implement a queue for failed records or retry mechanism
        logger.error('error processing disbursement request', err)
    }
}
