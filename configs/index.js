'use strict'

const convict = require('convict')

const conf = convict({
    aws: {
        secret_key: {
            env: 'AWS_SECRET_ACCESS_KEY',
            format: '*',
            default: 'secret_key',
        },
        key_id: {
            env: 'AWS_ACCESS_KEY_ID',
            format: '*',
            default: 'key_id',
        },
    },
    dynamo: {
        url: {
            env: 'AWS_DYNAMO_DB_URL',
            format: '*',
            default: 'http://localhost:8005',
        },
        region: {
            env: 'AWS_DYNAMO_DB_REGION',
            format: '*',
            default: 'localhost',
        },
    },
    sqs: {
        url: {
            env: 'AWS_SQS_URL',
            format: '*',
            default: 'http://localhost:9324',
        },
        region: {
            env: 'AWS_SQS_REGION',
            format: '*',
            default: 'sa-east-1',
        },
    },
    openkvk: {
        url: {
            env: 'OPENKVK_API_URL',
            format: '*',
            default: 'https://api.overheid.io',
        },
        key: {
            env: 'OPENKVK_API_KEY',
            format: '*',
            default: null,
        },
    },
})

conf.validate({ allowed: 'strict' })

module.exports = conf
