# Node.js Assignment (Serverless + eventing)

> We are going to (re)write a simple loan application, that will be split in two apps and communicate with each other through events & commands. The main goal of this assignment is to test your skills understanding, implementing new functionality, refactoring and writing tests. The current code is full of bad practices and inconsistencies, and it's your goal to make it shine, keeping it simple enough.

### Requirements

-   Node v8.10.0
-   Serverless.com CLI
-   Yarn (optional)

### Getting started

-   Install dependencies: `yarn install`
-   Install local dynamodb (required workaround): `serverless dynamodb install`
-   Run ElasticMQ (required for SQS plugin): `docker run -it -p 9324:9324 s12v/elasticmq:latest`
-   Run tests: `yarn test`
-   Run for development: `yarn start`
-   Check lint issues: `yarn lint`

### Technologies

-   Platform: Node.js
-   Programming language: Javascript (ES6) / Typescript
-   Framework: Serverless.com
-   Main AWS Services: Lambda, DybamoDB

## The assignment

-   Task 1: redesign the API and implement proper validations on inputs, with proper error messages and status code :heavy_check_mark:
-   Task 2: extend the create loan endpoint to also receive the `id` of the company on [openkvk](https://overheid.io/documentatie/openkvk). Only `active` companies should be allowed and you should store all information about the company on DynamoDB.       :heavy_check_mark:
-   Task 3: implement asyncronous disburse functionality (see instructions below) :heavy_check_mark:

## Improvements to be done
-   Documentation (API Blueprint and/or Swagger)
-   Logging (Provide better logs)
-   CI
-   Cache and Anti-corruption layers for third parties (OpenKVK)
-   Integration tests